.. MetricSubs documentation master file, created by
   sphinx-quickstart on Fri Jul 10 20:42:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. include:: central.rst


Indices and tables
------------------

.. toctree::
   :hidden:
   :maxdepth: 0
   :caption: 中心页
   :name: index_central

   central

.. toctree::
   :glob:
   :hidden:
   :maxdepth: 2
   :caption: MetricSubs
   :name: index_priv

   metricsubs/*

.. toctree::
   :glob:
   :hidden:
   :maxdepth: 2
   :caption: 新人手册
   :name: index_newbie

   newbieguide/*
   
.. toctree::
   :glob:
   :hidden:
   :maxdepth: 2
   :caption: 软件
   :name: index_soft

   softwares/*



* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

成员列表
=========

视频发布账号
-------------
这些账号用于字幕组发布视频：

- TechLinked: `@TechLinked <https://space.bilibili.com/326664284>`_
- 综合发布：`@MetricSubs <https://space.bilibili.com/40448581>`_

字幕组成员
-----------
这些是字幕组成员们的个人账号

活跃成员
---------
你看到的视频 就是他们的肝

- @MetricVoid: `[B站空间] <https://space.bilibili.com/7528074>`_ `[发邮件] <mailto:metricvoidlx@gmail.com>`_
- @远嘉: `[B站空间] <https://space.bilibili.com/13593761>`_
- @Benny `[B站空间] <https://space.bilibili.com/96861457>`_
- @A1ex_inamin `[B站空间] <https://space.bilibili.com/10572578>`_ `[微博] <https://space.bilibili.com/10572578>`_
- @audiESP `[B站空间] <https://space.bilibili.com/3965896>`_
- @DanielXD `[B站空间] <https://space.bilibili.com/12510919>`_
- @Percy a.k.a Pussy `[B站空间] <https://space.bilibili.com/255819375>`_
- @LittleStone a.k.a 结石/小结石 `[B站空间] <https://space.bilibili.com/93283131>`_


见习组员
--------
学习为主 烤肉为辅

- @Titanxz `[B站空间] <https://space.bilibili.com/8343097>`_
- @JohNnimOn `[B站空间] <https://space.bilibili.com/2285673>`_
- @Keanuo `[B站空间] <https://space.bilibili.com/776966>`_
- @Crtory `[B站空间] <https://space.bilibili.com/4669548>`_
- @葡萄噗桃浦 `[B站空间] <https://space.bilibili.com/29275615>`_
- @tyx2002 `[B站空间] <https://space.bilibili.com/10309503>`_
- @网络油侠 `[B站空间] <https://space.bilibili.com/10706866>`_ `[另一个B站空间] <https://space.bilibili.com/248582596>`_

不活跃成员
----------
之前做过视频，现在因学业繁忙等各种原因暂停的成员

- @zhubaohi `[B站空间] <https://space.bilibili.com/26309471>`_ 
- @Wangerry `[B站空间] <https://space.bilibili.com/7689576>`_
- @BazingA
- @极客小天 `[B站空间] <https://space.bilibili.com/259532402>`_
- @脑浆炸裂☆根 `[B站空间] <https://space.bilibili.com/72866>`_
- @浮在表面的bo `[B站空间] <https://space.bilibili.com/39752834>`_
- @Shelly [B站空间]
- @RCN路把图先生 `[B站空间] <https://space.bilibili.com/24688773>`_

英灵殿
------
无限期咕咕咕的成员

- @战术薇恩的目镜
- @西伯利亚Husky `[B站空间] <https://space.bilibili.com/66560652>`_
- @机智的宝鸽 `[B站空间] <https://space.bilibili.com/11043877>`_

已离职
------
曾属于字幕组，现已退出的成员
.. role:: strike
    :class: strike

FFMpeg
=======

ffmpeg是一个多功能的命令行媒体工具，我们主要用它压制视频。

下载
-----

- Windows: https://ffmpeg.zeranoe.com/builds/
- Linux: :strike:`你用Linux还要我教这个？` 
   - `在这里找发行版`_ 
   - `Arch Linux AUR`_: ffmpeg-git
   - `自己编译`_
- MacOS: 使用Homebrew :code:`brew install ffmpeg`

安装(?)
--------
说是安装，其实就是把ffmpeg放到PATH里。

Windows
~~~~~~~~
首先找一个中意的文件夹，把ffmpeg.exe放好（比如C:\\ffmpeg）

然后右键我的电脑 -> 属性 -> 高级系统设置 -> 高级(标签页) -> 环境变量。在“系统变量”中找到Path，编辑->新建，输入C:\\ffmpeg.

Linux
~~~~~~
使用包管理器的话，ffmpeg应该已经被安装到某一个 :code:`bin` 里了。使用 :code:`which ffmpeg` 查看ffmpeg的位置。

如果 :code:`which ffmpeg` 没有返回结果的话，可以直接把ffmpeg复制 :code:`/usr/bin` 下面，也可以找个位置放好之后 :code:`ln`。ffmpeg编译包含了所有的运行库，单文件即可运行。

MacOS
~~~~~~
Homebrew 永远滴神 :code:`brew install ffmpeg`


.. _`在这里找发行版`: https://ffmpeg.org/download.html#build-linux
.. _`Arch Linux AUR`: https://aur.archlinux.org/packages/ffmpeg-git/
.. _`自己编译`: https://git.ffmpeg.org/ffmpeg.git
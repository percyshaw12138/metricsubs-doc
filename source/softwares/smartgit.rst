SmartGit (git 图形客户端）
==========================

.. warning::
    此软件已经不再使用。

0、clone
---------

在操作仓库之前，得先把仓库下载到本地。有两种方法clone私有仓库。
此处推荐方法B.

1. Clone服务器上的库，方法A (直接登录)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

开放GitLab库权限之后，可以在”Your projects”下看到相应的库。

点击进入字幕组的库。右上角点击Clone，并在Clone with HTTPS的后面点击复制.

然后，在SmartGit的菜单栏中，选择Repositories -> Clone。弹出的窗口中粘贴地址，并确定

此时会弹出窗口。输入自己GitLab的用户名和密码。

Clone相当于将远程库保存到本地，因此需要选择本地保存路径。会自动在选定的目录下创建子文件夹，子文件夹名和Git库名相同。

2. Clone服务器上的库，方法B (OAuth)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
此方法稍微复杂些，但是又简单些。。。

直接打开SmartGit，选择Clone。点击右侧的小三角，选择Add Hosting Provider.

下拉菜单中选择GitLab，然后点击Generate Token。此时会打开浏览器。如果此时未登录则需要登录。然后选择Authorize，允许SmartGit作为api访问你的账户。

.. image:: ../_static/images/smartgit-9.png

获得一串Token。将它复制到SmartGit中，点击OK，再点击上一个窗口中的Add.

设置完毕后，SmartGit则可自动获得你账户上的项目，选择导入即可。之后步骤与A相同。

1、界面
--------

.. image:: ../_static/images/smartgit-1.png

左侧：文件结构树。它展示了这个Git库的文件结构。此处展示了所有文件夹，但不显示文件。

主窗口 – 上面：文件浏览器。此处仅显示变化的文件，而不是所有文件，所以刚clone完成的时候，这里是空荡荡的（因为没有做过任何修改）。

主窗口 – 中间：变化浏览器。在文件浏览器中选择发生变化的文件时，这里会显示更改的内容：左侧显示原来的文件，右侧显示现在的

主窗口 – 下面：Journal，当前库的所有commit。在clone库的时候，所有的commit也会被clone下来。在此处双击某个commit则可以打开log，显示所选commit的内容

举例，当我在一个库中做一些修改，它会变成这样：

.. image:: ../_static/images/smartgit-2.png

其中：

- Modified：文件被更改
- Missing：文件消失（被删除）
- Untracked：文件出现（新建）
- Renamed：重命名

当commit后就可以双击它打开Log。本次commit被记录如下：

.. image:: ../_static/images/smartgit-3.png

2、保存工作（发起Commit）
-------------------------

工作做了一些后，就可以commit了。

例如，我修改了样式，将几个Dialog变成了Comment:

.. image:: ../_static/images/smartgit-4.png

此时，在文件浏览器中选择所有需要commit的文件（根据需要，可以留一些暂时不commit，留到下次）。然后点击上面的Commit。在弹出的窗口中，确认需要commit的文件已经勾选（前面的复选框），然后填入Commit Message。如果点commit时没有选择文件的话，可以在这里勾选。

.. image:: ../_static/images/smartgit-5.png

完成后，Commit和Commit&Push都可以点击了。如果Commit以后希望先不上传更改，则点击Commit。如果想commit之后直接上传，则点击Commit & Push.

3、上传/下载保存的工作（Push和Pull）
------------------------------------

.. image:: ../_static/images/smartgit-6.png

左侧的Local Branches下，写着master 2> origin，代表本地主分支(master)比服务器主分支(origin/master)领先了两个commit. 本地领先服务器时，即可push.

在Journal中，黄色的代表本地的commit，黑色的代表服务器和本地都有的commit，绿色代表服务器上有，但是本地没有的commit。如果本地有commit还未push，可点击push，将本地的commit上传到服务器。

.. image:: ../_static/images/smartgit-7.png

此处显示的是master <2 origin，则表示本地主分支(master)比服务器主分支(origin/master)落后了两个commit。本地落后服务器时，即可pull.

.. image:: ../_static/images/smartgit-8.png

此处则表示，服务器上和本地分别做出了更改，因此造成origin/master和本地master分叉。如果更改的是不同的文件，则push/pull可照常进行。如果是相同文件，则需要进行rebase/merge。rebase/merge将在冲突篇中详细讲解。
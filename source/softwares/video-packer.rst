.. role:: strike
    :class: strike

Video-Packer
=============
video-packer是自己写的一个ffmpeg图形界面，包含了常用解码器的常用参数，并加入了从中间切广告的功能。

下载
-----
目前只支持Windows：https://github.com/Metric-Void/video-packer/releases

:strike:`我也很想支持Mac 但是我没有Mac 测都测不了`

使用
-----

.. warning::
    | 这是一个内部软件 :strike:`懒`，它不会检查参数的合法性。
    | 请自行确认参数，特别是速率控制的参数别忘了填。
    | **应用预设可以自动填写参数**

.. image:: ../_static/images/video-packer.png

把文件拖到文件框上即可快速输入路径。选择预设后点击“应用”（当然也可以修改参数），然后点击开始即可。
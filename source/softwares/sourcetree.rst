.. role:: strike
    :class: strike

SourceTree (git 图形客户端）
============================
SourceTree是一个完全免费的git图形客户端。

.. warning::
    此软件已经不再使用。
    
.. contents::

.. note::
   本页内容为MetricSubs特有。在他组时请多确认

安装
----
首先还是需要 `安装git命令行`_ 。这个软件会使用到git命令行。安装完成后前往 `SourceTree官网`_ 下载Windows或Mac版。

:strike:`啥 你用Linux？Linux还需要图形界面？`

安装过程中选择如下参数。

.. image:: ../_static/images/st-install-1.png

这一步直接选择跳过即可。我们用不到BitBucket。

.. image:: ../_static/images/st-install-2.png

这一步取消勾选Mercurial。它是另一个版本管理系统，我们用不到。

.. image:: ../_static/images/st-install-3.png

这一步是设置自己的用户名和邮箱。它和 ``git config --global user.name`` 以及 ``git config --global user.email`` 是一样的。
如果在安装git时设置过了，此时的窗口会自动填充你的设置。

.. image:: ../_static/images/st-install-4.png

除非你在注册GitLab时配置了ssh，选择否。

配置Personal Token
------------------
前往GitLab网页，右上角点击自己的头像->Settings，在左侧选择Access Tokens

.. image:: ../_static/images/gitlab-patoken.png

Name可以随便填一个，此处填了Source Tree API以方便记忆。Expiry Date不填则代表永久。权限选择api，即给予所有权限。

点击绿色的“Create Personal Access Token”，页面顶端会出现如下内容

.. image:: ../_static/images/gitlab-patoken-2.png

将此处的personal access token保存下来，回到SourceTree软件。在标签页的顶端选择“Remote”，在下方选择“添加一个账户”。选择GitLab如下，并点击“刷新Personal Access Token”

.. image:: ../_static/images/gitlab-patoken-3.png

此时会弹出如下的窗口。在“用户名”中输入的GitLab用户名（不是邮箱）。在“密码”中输入刚才生成的Personal Access Token（不是GitLab密码）。

.. image:: ../_static/images/gitlab-patoken-4.png

再次点击确认，窗口中会显示认证成功。

.. image:: ../_static/images/gitlab-patoken-5.png

克隆组仓库
----------
配置完Personal Access Token之后点击确定，回到Remote界面，点击右侧的“刷新”，即可列出账户拥有权限的库。
找到需要的库，并点击Clone。比如TechLinked就是 ``MetricSubs-TechLinked`` 。

.. image:: ../_static/images/st-clone.png

克隆时可以选择选项。比如可以选择只克隆前50个commit，避免下载过多内容。在“克隆深度”中选择50。
克隆完成之后，仓库会在当前标签页中打开。在新建的标签页中会出现在“Local”下面。

.. important::
    SourceTree有一个默认选项需要变更。在“工具->选项”中，找到Git标签页，并勾选“对跟踪分支默认使用变基替代合并”。


仓库操作
--------
.. image:: ../_static/images/st-interface.png

在左侧有几个视图，分别提供不同的功能。

- “WORKSPACE - 文件状态”： 展示本地文件发生的变化，并将其复制进暂存区。
- “WORKSPACE - History”： 展示仓库历史，以及各个commit包含的的变化。
- “WORKSPACE - Search”： 搜索commit。
- 左侧的其它视图暂时用不到。

上方的几个按钮也分别对应git操作。

- 提交: commit。使用暂存区的文件创建commit。
- 拉取：从远程仓库拉取新的commit并应用。相当于 ``git pull``
- 推送：将本地的新commit推送到远程仓库。相当于 ``git push``
- 获取：从远程仓库拉取新的commit，但暂时不应用。相当于 ``git fetch``


工作流程
--------
在工作前，点击“拉取”。这会将别人做出的改动拉取到本地。

在工作处理完成之后，发生变化的文件会出现在“文件状态”中。如图（此处使用了另一个仓库作为展示）。
使用加号或者上方的按钮将其移入或者移出暂存区，确认无误之后在下方输入提交信息，并点击“提交”。此时“推送”按钮上会出现一个气泡，代表本地有commit没有提交到远程。
点击“推送”即可提交。
 
.. image:: ../_static/images/st-stage.png

常见错误
--------
点击“提交”没有反应
    看看你的暂存区是不是空的。检查有没有输入Commit Message.

推送失败： [rejected] hint: Updates were rejected because the remote contains work that you do not have locally.
    远程仓库含有新的commit,但是你没有pull下来。点击“拉取”来同步远程仓库，然后再推送。

推送产生警告：CRLF will be converted to LF
    Windows和Linux的换行符问题，可以忽略。


.. _`安装git命令行`: git.html#下载、安装
.. _`SourceTree官网`: https://www.sourcetreeapp.com/